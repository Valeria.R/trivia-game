/*CPT Part 1
 * Disney Roulette
 * The Dice class
 * Made by Valeria and Iris
 * This is the testing class where we put all our classes together and developed the Disney Trivia Game.
 */

//For starters we imported the util and io class so we could use other classes as well as file reader
import java.util.*;
import java.io.*;

//Start of class
public class Disney 
	{
//This is a public static void, class
	public static void main(String[] args)
	{
		
		//To start we declared all the classes used in the game
		
		//Declare a file
		File file1 = new File("disney_castle copy.txt");
		
		//Declare a Scanner
		Scanner input = new Scanner(System.in);
		
		//Declare a Random 
		Random rnd = new Random();
		
		//Declare two players, player 1 and player 2
		player player1 = new player("name1");
		player player2 = new player("name2");
		
		//Declare a dice
		Dice dice = new Dice();
		
		//And finally declare the text class with all the messages used
		Text word = new Text();
		
		//These are the variables used throughout the game
		
		//The int variable "trivia" will be used to store a random number, for later calling up the trivia questions and answers
		int trivia;
		
		//This int variable "answer" will be used to store the number players type in as their answer to the questions.
		int answer;
		
		
		//START OF THE GAME
		
		//Welcome Message 
		System.out.println("Welcome to Disney Roulette!");
		
		
		
		//Without graphics to back up our game design, we chose to print out a disney castle logo we fixed up. 
		//To do this we planned it out on a text, and then used file reader to print out that text onto java.
		try
	    {
	          
	     //Declare file reader
	      FileReader x = new FileReader(file1);
	    
	     //declare a buffered reader
	      BufferedReader rd = new BufferedReader(x);
	      
	      //Read the first line of the text, save text in String variable "line"
	      String line = rd.readLine();
	      
	      //This while loop will go on as long as there is still text to read. 
	      while(line != null)
	      {
	       
	    //Print out the line read
	    System.out.println(line);
	    
	    //Then read the next line, and so on, until the whole text has been read.
	     line = rd.readLine();
	     
	     
	     
	      }
	      //Close the buffered reader
	      rd.close();
	    }
		
	      catch(IOException e)
		    {
		    }
		
		
		//After the logo is printed we started the game by allowing players to choose their avatar(player name)
		System.out.println("Let's start by choosing your avatars!");
		
		//Print asking player 1 to choose their avatar, aka, give themselves a name
		System.out.println("Please enter your avatar name player 1: ");
		//Then saving their string  answer in an "a" variable
		String a = input.nextLine();
		
		//And setting that player's name
		player1.setName(a);
		
		//Print asking player 2 to choose their avatar, aka, give themselves a name
		System.out.println("Please enter your avatar name player 2: ");
		
		//Then saving their string  answer in an "b" variable
		String b = input.nextLine();
		
		//And setting that player's name
		player2.setName(b);
		
		
		
		//These few lines of prints will display the rules of the game for players to read.
		System.out.println("Great! Here are the rules.");
		System.out.println("Each player starts off with 6 lives, for every round each player will roll a dice, ");
		System.out.println("that will be how much pixie dust you collect that round. ");
		System.out.println("The player with the least amount of pixie dust in every round looses a life.");
		System.out.println("If you land a 5 or 10 you get a Disney trivia with the chance to win a life back");
		System.out.println("If you answer wrong, you loose a life! Oh no!");
		System.out.println("The game will end when one player looses all their lives");
		System.out.println("");
		
		System.out.println("Good luck! Let the games begin!");
		
		
		
		//The start of the round while loop. The game will run only if both players still have lives
		while(player1.getLives() > 0 && player2.getLives() > 0 )
		{
			//This next part by seem a bit redundant, but I added it as a way for players to have more game interaction, since we couldn't use buttons.
			//For every round players are asked to enter the number 1 to start the round. 
			
			//This way there's a pause after every round, and not a continuous stream, and players have a chance to read their rounds.
			System.out.println("Enter 1 to start the round");
			
			//Their answer is saved under the int variable "click"
			int click = input.nextInt();
			
			//And only if they inputed a 1 will the round start, if not the loop will start again, prompting them again.
			if(click == 1)
			{
			
			
			//ROLLS
			//The method for rolling the dice is called
			dice.rollDice();
			
			//Add that roll number to player 1's sum
			player1.setSum(dice.roll);
			//Then print out the player's roll and their current sum.
			System.out.println(player1.getName()+ " you rolled a "+ dice.roll + ", now you have "+ player1.getSum() + " pixie dust");
			
			
			
			//CHECK TRIVIA
			
			//Players get the chance for a trivia question if they land on a 5 or 10
			if(dice.roll == 5 || dice.roll == 10 )
			{
				//There are 36 trivia questions, a random number is selected and saved under the trivia variable
				trivia = rnd.nextInt(36);
				
				//Print out the trivia question, calling up the question under the "trivia" position
				System.out.println("TRIVIA QUESTION: "+dice.getQuestion(trivia));
			    //The player's answer is then stored in the answer variable
				answer = input.nextInt();
				
				//Then the player's answer and the trivia(position) answer are compared
				if(answer == dice.getAnswer(trivia))
				{
					//If the answers match, the player is congratulated 
					System.out.println("Good job, "+ player1.getName()+ word.lifeBackText() +", you won a life");
					//and a life is added to their number
					player1.addLife();
				}
				else 
				{
					//If they don't match then a life is taken away
					player1.subLife();
					//And a message stating their error is printed, as well as the correct answer
					System.out.println("Wrong, "+ player1.getName()+ ", you lost a life. The correct answer was " + dice.getAnswer(trivia));
				}
		
				
			}
			//Print out a space to make it easier to read and a neater program.
			System.out.println("");
			
			//Then the dice is rolled again for the second player
			dice.rollDice();
			
			//And their sum is increased by their roll
			player2.setSum(dice.roll);
			
			//Their roll and current sum is printed
			System.out.println(player2.getName()+ " you rolled a "+ dice.roll + ", now you have "+ player2.getSum() + " pixie dust");
			
			//CHECK TRIVIA
			
			//Players get the chance for a trivia question if they land on a 5 or 10
			 if(dice.roll == 5 || dice.roll == 10 )
			{
				//There are 36 trivia questions, a random number is selected and saved under the trivia variable
					trivia = rnd.nextInt(36);
					
					//Print out the trivia question, calling up the question under the "trivia" position
					System.out.println("TRIVIA QUESTION: "+dice.getQuestion(trivia));
				    //The player's answer is then stored in the answer variable
					answer = input.nextInt();
				
				//Then the player's answer and the trivia(position) answer are compared
				if(answer == dice.getAnswer(trivia))
				{
					//If the answers match, the player is congratulated
					System.out.println("Good job, "+ player2.getName()+  word.lifeBackText() +", you won a life.");
					//and a life is added to their number
					player2.addLife();
				}
				else 
				{	
				//If they don't match then a life is taken away
				  player2.subLife();
				//And a message stating their error is printed, as well as the correct answer
					System.out.println("Wrong, "+ player2.getName()+ ", you lost a life. The correct answer was " + dice.getAnswer(trivia));
				}
			}
			
			//CHECK HERE IF SUMS ARE THE SAME
			 //To not be unfair, we decided to have players roll again, without the chance of trivia, if their sums are the same
			 //A while loop will run if the sums of both players are the same
			 while(player1.getSum() == player2.getSum())
			 {
				 //It'll start by informing players of the issue
				 System.out.println("Both Players have the same amount of pixie dust, meaning you get to roll again! Sorry, no trivia this time.");
				 
				 //Then player 1 will get to roll again
				 System.out.println(player1.getName()+ " you rolled a "+ dice.rollDice());
				 
				 //And their sum is increased by their roll
				 player1.setSum(dice.roll);
				 
				 //Then player two gets to roll
				 System.out.println(player2.getName()+ " you rolled a "+ dice.rollDice());
				 
				 //And their sum is also increased
				 player2.setSum(dice.roll);
				 
				 //It is only when their sums are not the same, that the loop will break, if not it will run again.
				 if(player1.getSum() != player2.getSum())
				 {
					 break;
				 }
				 
			 }
			
			//COMPARE SUMS 
			 //Now to see which player will loose the round.
			 
			 //We decided to store their current sums in an array list
			ArrayList<Integer> sums = new ArrayList<Integer>();
			//And set them as the first and second position
			sums.add(0, player1.getSum());
			sums.add(1, player2.getSum());
			
			//This int variable called lowest is set to an impossible high number in this game, and will be used to find the lowest value
			int lowest = 10000;
			
			//This for loop will run two times
			for(int counter = 0; counter < 2; counter++)
			{
				//It will go through the array, finding which value is lower
				if(sums.get(counter) < lowest)
				{
					//Once it finds a lower value, "lowest" is set to it
					lowest= sums.get(counter);
				}
			
			}
			//Print out a space to make it easier to read and a neater program.
			System.out.println("");
			
			//Then we used the index method of an array list to compare which position in the array is the lowest value
			 if(sums.indexOf(lowest) == 0)
			 {
				 //If it's in the first position then player one has the lowest sum and it looses a life
				 player1.subLife();
				 //A message is printed telling them of their loss
				 System.out.println("Sorry, "+ player1.getName() + word.deathText() +" you lost a life." );
				 
			 }
			//If it's in the second position then player two has the lowest sum and it looses a life
			 else if(sums.indexOf(lowest) == 1)
			 {
				 player2.subLife();
				//A message is printed telling them of their loss
				 System.out.println("Sorry, "+ player2.getName() + word.deathText() +" you lost a life."  );
				 
			 }
			//Print out a space to make it easier to read and a neater program.
			 System.out.println("");
		
			 //These two messages are a summary of the round, informing players the number of lives they have and their sum
			 System.out.println(player1.getName() + " you have " +player1.getLives() + " lives and, "+ player1.getSum() + " pixie dust!");
			 System.out.println(player2.getName() + " you have " +player2.getLives() + " lives and, "+ player2.getSum() + " pixie dust!");
				
			//Print out a space to make it easier to read and a neater program.
			 System.out.println("");
			 
			//CHECK IF ANYONE HAS ONLY 1 LIFE
			 //If player 1 has only 1 life left then they are given the chance to roll three times and win a life back
			if(player1.getLives() == 1)
			{
				//A message is printed informing them they have a chance
				System.out.println(player1.getName()+ ", You now have the chance to win one extra life! Roll three times and get a 5 for trivia! ");
				//A for loop will run three loops, since player only gets three chances
				for(int counter = 0; counter < 3; counter++)
				{
					//It is printed each of their rolls
					System.out.println("You rolled a " + dice.rollDice());
					//And after every roll it checks if the player rolls a 5. 
					//This time we changed it to only be 5, to decrease the chance, and up the stakes
					//If they rolled a 5...
					if(dice.roll == 5)
					{
						//It will get a random trivia position 
						trivia = rnd.nextInt(36);
						
						//And that trivia question will be presented to them.
						System.out.println("TRIVIA QUESTION: "+dice.getQuestion(trivia));
						//And their answer will be stored
						answer = input.nextInt();
						
						//The answers will be compared
						if(answer == dice.getAnswer(trivia))
						{
							//If they get it correct, they will get a life back text 
							System.out.println("Good job, "+ player1.getName()+  word.lifeBackText() +", you won a life");
							//And a life will be added to their number
							player1.addLife();
							
						}
						else 
						{
							//If not they will be informed of their error
							System.out.println("Wrong, "+ player1.getName()+ ", you did not win a life");
							//We decided not to take out a life, as to give them one final chance of making a come back
						}
						//Then the for look is broken, because they don't need to roll if they got the chance 
						break;
					}
					
					
				}
			}
			
			 //If player 2 has only 1 life left then they are given the chance to roll three times and win a life back
			if(player2.getLives() == 1)
			{
				//A message is printed informing them they have a chance
				System.out.println(player2.getName()+ ", You now have the chance to win one extra life! Roll three times and get a 5 for trivia! ");
				//A for loop will run three loops, since player only gets three chances
				for(int counter = 0; counter < 3; counter++)
				{
					//It is printed each of their rolls
					System.out.println("You rolled a " + dice.rollDice());
					//And after every roll it checks if the player rolls a 5. 
					//This time we changed it to only be 5, to decrease the chance, and up the stakes
					//If they rolled a 5...
					if(dice.roll == 5)
					{
						//It will get a random trivia position 
						trivia = rnd.nextInt(36);
						
						//And that trivia question will be presented to them.
						System.out.println("TRIVIA QUESTION: "+dice.getQuestion(trivia));
						//And their answer will be stored
						answer = input.nextInt();
						
						//The answers will be compared
						if(answer == dice.getAnswer(trivia))
						{
							//If they get it correct, they will get a life back text 
							System.out.println("Good job, "+ player2.getName()+  word.lifeBackText() +", you won a life");
							//And a life will be added to their number
							player2.addLife();
							
						}
						else 
						{
							//If not they will be informed of their error
							System.out.println("Wrong, "+ player1.getName()+ ", you did not win a life");
							//We decided not to take out a life, as to give them one final chance of making a come back
						}
						//Then the for look is broken, because they don't need to roll if they got the chance 
						break;
					}
					
					
				}
			}
			//Print out a space to make it easier to read and a neater program.
			System.out.println("");
			}
			
			//Finally, that is all the coding for the rounds. 
			//This final else statement belongs to the every first if statemet in the main while loop
			else
			{
				//If a player does not input a 1 to start the round then it will give them this message and the while loop will start again.
				System.out.println("That's not a 1, silly. Try again!");
			}
			
			
			
		}
		
		//It is once a player has no more lives left that the main loop will break and go through these final messages 
			//Print out a space to make it easier to read and a neater program.
			System.out.println("");
			
			//Message for players
			System.out.println("The results are in!");
			
			//If it is player 1 with 0 or less lives then...
			if(player1.getLives() <= 0 )
			{
				//Player 2 gets the congratulations and victory message 
				System.out.println("Congratulations, "+ player2.getName()+ word.victorytext() + " and won the game!");
				
				//And player 1 gets the death message
				System.out.println("Sorry, "+ player1.getName()+ word.deathText()+ " and lost the game.");
			}
			
			//If it is player 2 that has 0 or less lives
			 if(player2.getLives() <= 0)
			{
				//Player 1 gets the congratulations and victory message 
				System.out.println("Congratulations, "+ player1.getName()+ word.victorytext()+ " and won the game!");
				//And player 2 gets the death message
				System.out.println("Sorry, "+ player2.getName()+ word.deathText()+ " and lost the game.");
			}
			
			 //Finally print out game over to announce the end of the game. 
			 System.out.println("Game over!");
		
		
		//End of program
	
		
	}

}