/*CPT Part 3
 * Disney Roulette
 * The Text class
 * Made by Valeria and Iris
 * This third class contains all the methods to print messages throughout the game.
 */

//For starters we imported both the util class.
import java.util.*;


//Start of class
public class Text 
{
//Then declare the random object
	Random rnd = new Random();
	
	//instantaneous variables. This one is an int called message
	//Which will be used to know which text to output
	int message;

//Throughout the game we wanted to include Disney themed messages and text. 
//So we created a class that stores the long lists of messages we MADE up, organized into categories. 
	
//For starters is the "lifeBackText" class. 
//When a player gets a life back we created Disney messages that respond of a player's victory
public String lifeBackText()
{
	//The variable message will store a random number from 0-11. Which is how many messages we came up with.
	message = rnd.nextInt(12);
	
	//This string array list is composed of all the Life Back messages. 
	ArrayList<String> life = new ArrayList<String>();
	
	//We used the .add() array list method to add each string into the arrayList life.
	life.add(" Iron man saved you");
	life.add(" Spider-man saved you ");
	life.add(" Fairy-god mother came");
	life.add(" Genie gave you a wish");
	life.add(" King triton un-cursed you");
	life.add(" Baymax nursed you back to health");
	life.add(" You feel the force flow through you");
	life.add(" You find Mr. PotatoHead’s eyes and he grants you a favour ");
	life.add(" You received true love’s kiss");
	life.add(" You found vibranium to protect you");
	life.add(" Captain Marvel came to the rescue");
	life.add(" Rapunzel healed you with her magic hair");

	//Then it will return the string that's in the position "message" in the arrayList life. 
	//Basically it will output a random text from the list
	return life.get(message);
}

//The "deathtext" class is similar to the first one. 
//This one will be used to hold all the messages we came up with to use when a player loses a life, or loses the game.
public String deathText()
{
	//The variable message will store a random number from 0-16. Which is how many messages we came up with.
	message = rnd.nextInt(17);
	
	//This string array list is composed of all the death messages. 
	ArrayList<String> death = new ArrayList<String>();
	
	//We used the .add() array list method to add each string into the arrayList life.
	death.add(" You jumped into a fire to save Baymax");
	death.add(" You fell off your magic carpet");
	death.add(" Darth Vader used the force on you");
	death.add(" Maleficent cursed you");
	death.add(" Scar pushed you off a cliff");
	death.add(" Thanos snapped you out of existence");
	death.add(" You made a deal with Ursula");
	death.add(" You are one of the Evil Queen’s apples");
	death.add(" Cruella de Vil made you into a coat");
	death.add(" Hades sent you to Hell");
	death.add(" Jafar pushes you into the Cave of Wonders");
	death.add(" You lost too many balloons and your house sank");
	death.add(" You got turned into a frog by Dr. Facilier");
	death.add(" Elsa froze your heart");
	death.add(" Loki tricked you");
	death.add(" Hulk accidentally crushes you");
	death.add(" Aliens mistake you for Stitch and takes you");

	
	//Then it will return the string that's in the position "message" in the arrayList death. 
		//Basically it will output a random text from the list
	return death.get(message);
}

//This final method we created will store all the messages that can print out when a player wins the game
public String victorytext()
{
	//The variable message will store a random number from 0-16. Which is how many messages we came up with.
	message = rnd.nextInt(17);
	
	//This string array list is composed of all the victory messages. 
	ArrayList<String> victory = new ArrayList<String>();
	
	//We used the .add() array list method to add each string into the arrayList life.
	victory.add(" You lived happily ever after");
	victory.add(" You found the genie and");
	victory.add(" You found your Prince(ss) Charming");
	victory.add(" You escaped the rabbit hole");
	victory.add(" You made the perfect ratatouille");
	victory.add(" You wished upon a star, and it came true");
	victory.add(" You defeated the Empire");
	victory.add(" You brought the heart back to Te Fiti");
	victory.add(" You won the Piston Cup Race");
	victory.add(" You became a god again");
	victory.add(" You became a god again");
	victory.add(" You found Nemo");
	victory.add(" You helped the Incredibles save the city");
	victory.add(" You became king/Queen of the Jungle");
	victory.add(" You defeat the Huns");
	victory.add(" You made it back to the land of the living ");
	victory.add(" You became a super soldier");
	victory.add(" You reach Neverland");


	//Then it will return the string that's in the position "message" in the arrayList victory. 
	//Basically it will output a random text from the list
	return victory.get(message);
}

//End of class
}
