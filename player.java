/*CPT Part 2
 * Disney Roulette
 * The Player class
 * Made by Valeria and Iris
 * This second class contains all the methods a player can do.
 */

//Start of class
public class player 
{
	//instantaneous variables. 
	//We have the string variable called name, which will hold the name of the player
	 String name;
	 
	 //The int variable called lives, which will hold how many lives the player has
	  int lives;
	  
	  //The int variable called sum, which will hold how much "pixie dust" the player has collected
	  int sum;
	  
	  //The constructor for a player needs to include a name in the parameters, a
	  public player(String a)
	  {
		  //The name of the player will be set as a
	    name= a;
	    //their lives start out as being 6
	    lives=6;
	  }
	  
	  //This void method will be used to change the name of a player, starting with the parameters demanding a string a
	  public void setName (String a)
	  {
		 //The name of the player will be set as a
	    name=a;
	  }
	  
	  //This method will be used to output the name of the player
	  public String getName()
	  {
		  //It will return the name variable
	    return name;
	  }
	  
	  //This void method will be used to change the sum of the player, the parameters asking for a number
	  public void setSum (int roll)
	  {
		  //That number is then added to the player's sum
	    sum=sum+roll;
	  }
	  
	  //This method will be used to get the sum 
	  public int getSum()
	  {
		  //it will return the variable sum
	    return sum;
	  }
	  
	  //This void method will be used to add a life of the players life count
	  public void addLife()
	  {
	    //when the method is used the player's number of lives will increase by one.
	      lives=lives+1;
	    
	  }
	  
	  //This void method will follow the same pattern as addLife except here it'll reduce by one
	  public void subLife ()
	  {
		//when the method is used the player's number of lives will decrease by one.
	      lives = lives - 1;
	    
	  }
	  
	 
	 //This method returns the number of lives a player has
	  public int getLives()
	  {
		  //Returning the variable lives
	    return lives;
	  }


	//End of class 
	  
	  
}