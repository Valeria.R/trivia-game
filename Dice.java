/*CPT Part 1
 * Disney Roulette
 * The Dice class
 * Made by Valeria and Iris
 * This first class contains the dice methods to be used int the game
 */

//For starters we imported random, as it will be used to act as a dice
import java.util.Random;

//Start of class
public class Dice 
	{
	//Declare random class
	  Random rnd= new Random ();
	  //instantaneous variables. In this class the int variable roll is declared, will be used to hold the dice number
	  int roll;
	  
	  //Class methods
	  
	  //This method will be used to roll the 10 sided dice
	  public int rollDice()
	  {
	    //will output a random number between 1-10, which will simulate a 10 sided dice
	    return roll  = rnd.nextInt(10)+1;
	  }
	  
	//This next method will be used to store and give a trivia question(string) depending on the index given in the parameters
	  public String getQuestion(int a)
	  {
//We started by brainstorming questions.
//We used an array to sort out all the questions, and but them in a fixed order. This way it will perfectly align with the answers.
//We made sure to create unique Disney themed questions. They range from all the franchise movies, in form of multiple choice or true/false
//We tried our best to make up a list of questions that contained easy and hard choices. 
String [] questions = new String [36] ;
questions[0] = "How many dwarfs are there in Snow White: 4, 9, 10, 7, 2, 6";
questions[1] = "How many dalmatians does Cruella de Vil need: 101, 98, 100, 99, 97, 102 " ;
questions[2] = "In Aladdin what’s the name of Jasmine’s tiger: 1-Eeyore, 2-Rajah, 3-Zazu, 4-Rafiki, 5-Lucifer"; 
questions[3] = "In the Lion King, where does Mufasa and his family live: 1-Pride Rock, 2-Arendale, 3-Mount Olympus, 4-Crystal Rock, 5-Africa " ;
questions[4] = "Captain America became a super soldier in what year: 1-WW1, 2-WW2, " ;
questions[5] = "Who was the first Disney Princess: 1-Belle, 2-Jasmine, 3-Cinderella, 4-Snow White, 5-Aurora " ;
questions[6] = "Who’s the first female Marvel hero who got their own film: 1-Black Widow, 2-Captain Marvel, 3-Scarlet witch, 4-Mantis" ;
questions[7] = "Which Disney Princess wears pants: 1-Cinderella, 2- Snow White, 3-Jasmine, 4-Tiana, 5-Moana" ;
questions[8] = "What country is Belle from Beauty and the Beast from: 1-Italy, 2-Greenland, 3-France, 4-England, 5-Spain " ;
questions[9] = "True or False, Princess Lea and Luke Skywalker are cousins: 1-true, 2-False" ;
questions[10] = "What is Stitch from Lilo and Stitch: 1-Experiment, 2-Alien, 3-dog, 4-monster " ;
questions[11] = "Ursula turns into Vanessa to steal Prince Eric 1-true, 2- false" ;
questions[12] = "How many live-action versions of Spider-man are there: 5, 7, 2, 1, 3 " ;
questions[13] = "What does Merida’s mom from Brave turn into: 1-dog, 2-fairy, 3-monster, 4-Wolf, 5-bear" ;
questions[14] = "True or False, John Smith gets killed in Pocahontas: 1-true, 2- false" ;
questions[15] = "True or False, in Frozen Elsa was originally set to be the villain: 1-true, 2-false" ;
questions[16] = "What is Tiana’s signature dish: 1-beignets, 2-croissant 3-baguette, 4- cheesecake " ;
questions[17] = "“When will my life begin” song is from what movie: 1-Beauty and the Beast, 2-Finding Dory, 3-Moana, 4-Tangled, 5-Little Mermaid " ;
questions[18] = "What cities were the inspiration for the Big Hero 6 city: 1- Toronto/LA, 2- Sydney/Florida, 3- Tokyo/San Franciso, 4-Venice/Paris, 5- Chicago/London " ;
questions[19] = "“Reflection” song is from what movie: 1-Lion King, 2-Cinderella, 3-Pocahontas, 4- Moana, 5-Mulan " ;
questions[20] = "Who voice acts the Genie in Aladdin: 1-Paul Rudd, 2-John Cena, 3-Will Smith, 4- Robin Williams, 5-Robert Green" ;
questions[21] = "What movie is the villain Dr. Facilier from: 1-Snow white, 2-Princess and the Frog, 3-Brave, 4-Up" ;
questions[22] = "How many Star Wars movies are there: 7, 9, 10, 12, 6, 8 " ;
questions[23] = "Who had the first Marvel movie: 1-Thor, 2-Hulk, 3-Iron Man, 4-Captain America, 5-Nick Fury " ;
questions[24] = "Which Superhero was born first: 1-Iron man, 2-Captain America, 3-Captain Marvel, 4-Hawkeye" ;
questions[25] = "What is the name of the dog from Up: 1-Tom, 2-Peter, 3-Jerry, 4-Dug, 5-Max, 6-Bolt";
questions[26] = "What are the names of the chipmunks from Mickey Mouse: 1-Chip/Dale, 2-Tom/Jerry, 3-Alvin/Simon/Theodore 4-Goofy/Pluto" ;
questions[27] = "What was the name of the creator of Disney: 1-Welsh, 2-Walker, 3- Walt, 4 -William, 5- Winston" ;
questions[28] = "How many Toy Story movies are there: 1, 2, 3, 4, 5, 6, " ;
questions[29] = "What movie is the dog “Littler Brother” from: 1-Cinderella, 2-Mulan, 3-Toy story, 4-Moana, 5-Up" ;
questions[30] = "What movie is the character “Miguel” from: 1-Toy Story, 2-Cars 3, 3-Big Hero 6, 4-Monsters inc, 5-Coco " ;
questions[31] = "What movie is the character “Sully” from: 1-Little Mermaid, 2-Cars, 3-Monsters inc, 4-Zootopia " ;
questions[32] = "Which movie does NOT have animals as the protagonists: 1-Zootopia, 2-Finding Nemo, 3-Lady, and the Tramp, 4-Onward, 5-Ice age " ;
questions[33] = "Which movie is NOT Pixar: 1-Onward, 2-Inside out, 3- Coco, 4-Finding Dory, 5-Bolt";
questions[34] = "Which of the following did NOT get a squeal: 1-Hercules, 2-Beauty, and the Beast, 3-Cinderella, 4-frozen, 5-Tarzan " ;
questions[35] = "Which movie is “Timothy. Q Mouse” from: 1-Robin Hood, 2-Pinocchio, 3-Chicken little, 4-Treasure Planet, 5-Dumbo" ;

//The method will return the position of a in the array
	  return questions[a];
	}

	  //The method that stores the answers for the trivia is formed in a similar way.
	  //It too, is an array(int) 
	  public int getAnswer(int a)
	  {
		  //The array holds all the correct answers to the questions in the same order to fit the questions
		  int [] answers = new int[]{7, 99, 2, 1, 2, 4, 2, 3, 3, 2, 1, 2, 3, 5, 2, 1, 1, 4, 3, 5, 4, 2, 9, 3, 2, 4, 1, 3, 4, 2, 5, 3, 4, 5, 1, 5};
		  
		  //Then it will return the position of a in the array
		  return answers[a];
	  }
	  
	//End of class
}